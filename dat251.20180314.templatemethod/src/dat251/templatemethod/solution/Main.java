package dat251.templatemethod.solution;

public class Main {

	public static void main(String[] args) {
		
		int[] results = {23, 4, 56, 12, 78, 93, 9, 18};
		
		TopThree tt = new TopThreeUsingBubbleSort(results);
//		TopThree tt = new TopThreeUsingSelectionSort(results);
		
		System.out.println("GOLD:   " + tt.getGoldScore());
		System.out.println("SILVER: " + tt.getSilverScore());
		System.out.println("BRONZE: " + tt.getBronzeScore());

	}

}
