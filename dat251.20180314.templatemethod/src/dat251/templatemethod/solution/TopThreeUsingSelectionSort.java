package dat251.templatemethod.solution;

public class TopThreeUsingSelectionSort extends TopThree {

	public TopThreeUsingSelectionSort(int[] scores) {
		super(scores);
	}

	@Override
	protected void sort() {
        for (int nSorted=0; nSorted<scores.length; nSorted++) {
            
            int indexMin = nSorted;
            int min = scores[nSorted];
            
            for (int i=nSorted; i<scores.length; i++) {
                
                if (scores[i] < min) {
                    min = scores[i];
                    indexMin = i;
                }
            }
            
            scores[indexMin] = scores[nSorted];
            scores[nSorted] = min;
        }
	}
}
