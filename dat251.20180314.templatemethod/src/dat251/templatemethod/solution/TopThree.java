package dat251.templatemethod.solution;

public abstract class TopThree {
	
	int[] scores;
	
	public TopThree(int[] scores) {
		this.scores = scores;
	}

	//Missing/deferred method
	protected abstract void sort();
	
	//Hook method
	protected void swap(int index1, int index2) {
		int temp = scores[index1];
		scores[index1] = scores[index2];
		scores[index2] = temp;
	}

	//Template method
	public int getGoldScore() {
		sort();
		return scores[scores.length-1];
	}
	
	//Template method
	public int getSilverScore() {
		sort();
		return scores[scores.length-2];
	}
	
	//Template method
	public int getBronzeScore() {
		sort();
		return scores[scores.length-3];
	}
}
