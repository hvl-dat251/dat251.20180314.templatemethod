package dat251.templatemethod.solution;

public class TopThreeUsingBubbleSort extends TopThree {

	public TopThreeUsingBubbleSort(int[] scores) {
		super(scores);
	}

	@Override
	protected void sort() {

		for (int nSorted = 0; nSorted < scores.length; nSorted++) {
			for (int i = 0; i < (scores.length - nSorted - 1); i++) {
				if (scores[i] > scores[i + 1]) {
					swap(i, i+1);
				}
			}
		}
	}
}
